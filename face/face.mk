# Face unlock related configuration
PRODUCT_PACKAGES += \
    FaceUnlockTrafficLightOverlay

BOARD_VENDOR_SEPOLICY_DIRS += \
    vendor/google/pixelparts/sepolicy/face
