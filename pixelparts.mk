$(call inherit-product, vendor/google/pixelparts/touch/device.mk)

# Camera
PRODUCT_BROKEN_VERIFY_USES_LIBRARIES := true
PRODUCT_PACKAGES += \
    GoogleCamera

PRODUCT_VENDOR_LINKER_CONFIG_FRAGMENTS += \
    vendor/google/pixelparts/camera/linker.config.json

ifeq ($(PRODUCT_SUPPORTS_CLEAR_CALLING), true)
PRODUCT_PACKAGES += \
    ClearCallingSettingsOverlay
endif

# Dolby
BOARD_VENDOR_SEPOLICY_DIRS += \
    vendor/google/pixelparts/sepolicy/dolby

# Health
DEVICE_FRAMEWORK_COMPATIBILITY_MATRIX_FILE += vendor/google/pixelparts/health/framework_compatibility_matrix.xml
TARGET_HEALTH_CHARGING_CONTROL_SUPPORTS_BYPASS := false
TARGET_HEALTH_CHARGING_CONTROL_SUPPORTS_DEADLINE := true
TARGET_HEALTH_CHARGING_CONTROL_SUPPORTS_TOGGLE := false

PRODUCT_PACKAGES += \
    vendor.lineage.health-service.default

BOARD_VENDOR_SEPOLICY_DIRS += \
    vendor/google/pixelparts/sepolicy/health

# Parts
PRODUCT_PACKAGES += \
    GoogleParts \
    PixelFrameworksOverlay

# Repair mode
SYSTEM_EXT_PRIVATE_SEPOLICY_DIRS += \
    vendor/google/pixelparts/sepolicy/repair_mode

# Turbo
SYSTEM_EXT_PUBLIC_SEPOLICY_DIRS += \
    hardware/google/pixel-sepolicy/turbo_adapter/public

# UWB
BOARD_VENDOR_SEPOLICY_DIRS += \
    vendor/google/pixelparts/sepolicy/uwb

# Widevine
PRODUCT_VENDOR_LINKER_CONFIG_FRAGMENTS += \
    vendor/google/pixelparts/widevine/linker.config.json
